package org.iot.spark.util

object GeoDistanceCalculator {

  def GetDistance(lat1:Double,lon1:Double,lat2:Double,lon2:Double):Double = {
    val r:Int = 6371
    val latDistance:Double = Math.toRadians(lat2 - lat1); //角度转弧度
    val lonDistance:Double = Math.toRadians(lon2 - lon1);
    val a:Double = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1)) *
      Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2)
    val c:Double = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    val distance:Double = r * c
    return distance
  }

  def isInPOIRadius(currentLat:Double, currentLon:Double,poiLat:Double,poiLon:Double,radius:Double):Boolean = {
    val distance:Double = GetDistance(currentLat,currentLon,poiLat,poiLon)
    if (distance <= radius){
      return true
    }
    return false
  }
}
