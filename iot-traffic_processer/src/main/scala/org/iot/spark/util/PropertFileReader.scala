package org.iot.spark.util
import java.io.InputStream
import java.util.Properties

import org.apache.log4j.Logger

object PropertFileReader{
  private val logger:Logger = Logger.getLogger(classOf[PropertFileReader])
  private val prop:Properties = new Properties()

  def readPropertyFile():Properties ={
    if (prop.isEmpty){
      val input:InputStream = PropertFileReader.getClass.getClassLoader.getResourceAsStream("iot-spark.properties")
      try{
        prop.load(input)
      }catch {
        case e:Exception => println(e)
      }finally {
        if (input != null){
          input.close()
        }
      }
    }
    prop
  }

}

class PropertFileReader {

}
