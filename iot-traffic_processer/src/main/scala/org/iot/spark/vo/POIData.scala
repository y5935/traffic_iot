package org.iot.spark.vo

case class POIData(
  latitude:Double,
  longitude:Double,
  radius:Double)
